import { Component, OnInit } from '@angular/core';
import { CollectionService } from '../collection.service';
import { DomSanitizer, SafeResourceUrl, SafeUrl, SafeStyle } from '@angular/platform-browser';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit {

  ownedNr: any = [[]]
  allNr: any = [[]]
  spent: any = [[]];

  constructor(private collectionService: CollectionService, private _sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.getOwned();
    this.getAv();
    this.getMoney();
  }

  getMoney(): void {
    this.collectionService.getSpent().subscribe(sum => {
      console.log(sum)
      this.spent = sum
    });
  }

  getOwned(): void {
    this.collectionService.getOwnedNr().subscribe(numbers => {
      console.log(numbers)
      this.ownedNr = numbers
    });
  }

  getAv(): void {
    this.collectionService.getAvNr().subscribe(numbers => {
      console.log(numbers)
      this.allNr = numbers
    });
  }

  calcAll(): Number {
    let tsum = 0;
    for (let sum of this.allNr) {
      tsum += sum[0]; 
    }
    return tsum;
  }

  calcOwned(): Number {
    let tsum = 0;
    for (let sum of this.ownedNr) {
      tsum += sum[0]; 
    }
    return tsum;
  }

  calcSpent(): Number {
    let tsum = 0;
    for (let sum of this.spent) {
      tsum += sum[1]; 
    }
    return tsum;
  }

  getOwnedNr(name: String): Number {
    for (let x of this.ownedNr) {
      if (x[1] == name) {
        return x[0];
      }
    }
    return 0;
  }

  getProc(v1, v2): Number {
    return (v1 / v2) | 0;
  }

  sanit(v1, v2): SafeStyle {
    let p = this.getProc(v1, v2);
    return this._sanitizer.bypassSecurityTrustStyle(`${p}%`);
  }
}
