import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Sneaker } from './Sneaker';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class CollectionService {
  url: String = "http://localhost:5000"
  constructor(private http: HttpClient) { }

  public getAll() : Observable<Sneaker[]> {
    return this.http
      .get<Sneaker[]>(this.url + "/collection")
      .pipe(tap(data => console.log()),
            (catchError(this.handleError)));
  }
  
  public getColByProd(prod): Observable<Sneaker[]> {
    return this.http
      .get<Sneaker[]>(this.url + "/collection/prod/" + prod)
      .pipe(tap(data => console.log()),
            (catchError(this.handleError)));
  }

  public getAllAvailable() : Observable<Sneaker[]> {
    return this.http
      .get<Sneaker[]>(this.url + "/available")
      .pipe(tap(data => console.log()),
            (catchError(this.handleError)));
  }
  
  public getAllByProd(prod): Observable<Sneaker[]> {
    return this.http
      .get<Sneaker[]>(this.url + "/available/prod/" + prod)
      .pipe(tap(data => console.log()),
            (catchError(this.handleError)));
  }
  public addSneaker (sneaker: Sneaker): void {
      let httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': 'my-auth-token'
        }),
        body: sneaker
      }
      this.http.
      post<Sneaker>(this.url + "/collection", httpOptions)
      .subscribe(res => console.log("Worked"));
  }

  public getOwnedNr(): Observable<any> {
    return this.http
      .get<any>(this.url + "/collection/nr")
      .pipe(tap(data => console.log()),
            (catchError(this.handleError)));
  }

  public getAvNr(): Observable<any> {
    return this.http
      .get<any>(this.url + "/available/nr")
      .pipe(tap(data => console.log()),
            (catchError(this.handleError)));
  }

  public getSpent(): Observable<any> {
    return this.http
      .get<any>(this.url + "/collection/spent")
      .pipe(tap(data => console.log()),
            (catchError(this.handleError)));
  }

  private handleError(err: HttpErrorResponse) {
 
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {

        errorMessage = `An error occurred: ${err.error.message}`;
    } else {

        errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }


}
