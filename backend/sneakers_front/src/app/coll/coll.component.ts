import { Component, OnInit } from '@angular/core';
import { Sneaker } from '../Sneaker';
import { CollectionService } from '../collection.service';

@Component({
  selector: 'app-coll',
  templateUrl: './coll.component.html',
  styleUrls: ['./coll.component.css']
})
export class CollComponent implements OnInit {

  collection: Sneaker[] = []
  producers: String[] = [
    "Nike", 
    "Nike SB",
    "Adidas",
    "Adidas Originals",
    "Puma",
    "Jordan",
    "Asics"
  ]
  constructor(private collectionService: CollectionService) { }

  ngOnInit() {
    this.getSneakers();
    console.log(this.collection);
  }

  getSneakers() {
    this.collectionService.getAll().subscribe(sneakers => 
      sneakers.forEach(element => this.collection.push(new Sneaker(
        element[0], element[1], element[2], element[3]
    ))));
  }

  addSneaker(sneaker: Sneaker): void {
    // console.log(sneaker);
    this.collectionService.addSneaker(sneaker);
  }
  test(): void {
    this.collection = []
  }
  getBy(prod: String): void {
    this.collectionService.getColByProd(prod).subscribe(sneakers => {
      this.collection = []
      sneakers.forEach(element => this.collection.push(new Sneaker(
        element[0], element[1], element[2], element[3]
    )))});
  }
}
