import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'bd-frontend';
  all: Boolean = true;
  owned: Boolean = false;
  stats: Boolean = false;

  showCollection(): void {
    this.all = false;
    this.owned = true;
    this.stats = false;
  }

  showAll(): void {
    this.all = true;
    this.owned = false;
    this.stats = false;
  }

  showStats(): void {
    this.all = false;
    this.owned = false;
    this.stats = true;
  }
}
