import { Component, OnInit } from '@angular/core';
import { Sneaker } from '../Sneaker';
import { CollectionService } from '../collection.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  collection: Sneaker[] = []

  constructor(private collectionService: CollectionService) { }
  producers: String[] = [
    "Nike", 
    "Nike SB",
    "Adidas",
    "Adidas Originals",
    "Puma",
    "Jordan",
    "Asics"
  ]
  ngOnInit() {
    this.getSneakers();
    console.log(this.collection);
  }

  getSneakers() {
    this.collectionService.getAllAvailable().subscribe(sneakers => 
      sneakers.forEach(element => this.collection.push(new Sneaker(
        element[0], element[1], element[2], element[3]
    ))));
  }

  addSneaker(sneaker: Sneaker): void {
    console.log(sneaker);
    this.collectionService.addSneaker(sneaker);
  }
  getBy(prod: String): void {
    this.collectionService.getAllByProd(prod).subscribe(sneakers => {
      this.collection = []
      sneakers.forEach(element => this.collection.push(new Sneaker(
        element[0], element[1], element[2], element[3]
    )))});
  }
}
