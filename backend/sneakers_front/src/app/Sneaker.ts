export class Sneaker {
    constructor(
        public producer: String,
        public model: String,
        public color: String,
        public price: Number) {}
}