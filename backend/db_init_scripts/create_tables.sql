use sneaker_collection;

CREATE TABLE Sneakers (
    id INT PRIMARY KEY,
    producer_id INT,
    model_id INT,
    colorway_id INT
);

CREATE TABLE Producers (
    id INT PRIMARY KEY,
    producer VARCHAR(20)
);

CREATE TABLE Model (
    id INT PRIMARY KEY,
    model VARCHAR(20),
    producer_id INT
);

CREATE TABLE Colorways (
    id INT PRIMARY KEY,
    model_id INT,
    colorway VARCHAR(20),
    price INT
);

CREATE TABLE Spent (
    producer_id INT PRIMARY KEY,
    total INT
);

-- SCRIPTS

-- trigger to add the sum spent on sneakers from a producer
DELIMITER //

drop trigger if exists add_to_sum;
create trigger add_to_sum
    after insert on Sneakers
    for each row
BEGIN
    declare to_add_price INT;
    select price into to_add_price from Colorways where id=new.colorway_id;

    update Spent
    set total=total+to_add_price
    where producer_id=new.producer_id;
END //

DELIMITER ;


DELIMITER //

drop function if exists get_producer_name;
create function get_producer_name(prod VARCHAR(20))
returns VARCHAR(20)
BEGIN
    return (select id from Producers where prod=producer);
END //

DELIMITER ;

-- Gets all the collection
DELIMITER //
drop procedure if exists get_collection;
create procedure get_collection()
begin
    select p.producer, m.model, c.colorway, c.price 
    from 
        Sneakers s,
        Model m,
        Producers p,
        Colorways c
    where 
        s.producer_id=p.id AND
        s.model_id=m.id AND
        s.colorway_id=c.id;
end //

DELIMITER ;

-- Add a new entry in the collection
DELIMITER //
drop procedure if exists add_sneakers;
create procedure add_sneakers(IN s_id INT, IN prod VARCHAR(20), IN mode VARCHAR(20), IN colo VARCHAR(20))
begin
    declare p_id INT;
    declare m_id INT;
    declare c_id INT;

    select id into p_id from Producers where producer=prod;
    select id into m_id from Model where mode=model;
    select id into c_id from Colorways where colo=colorway;

    insert into Sneakers(id, producer_id, model_id, colorway_id)
    values (s_id, p_id, m_id, c_id);
end //

DELIMITER ;

-- Get by producer
DELIMITER //
drop procedure if exists get_by_prod;
create procedure get_by_prod(IN prod VARCHAR(20))
begin
    -- TODO: Use function here
    declare prod_id INT;
    select id into prod_id from Producers where producer=prod;

    select p.producer, m.model, c.colorway, c.price 
    from 
        Sneakers s,
        Model m,
        Producers p,
        Colorways c
    where 
        s.producer_id=p.id AND
        s.model_id=m.id AND
        s.colorway_id=c.id AND
        prod_id=s.producer_id;
end //

DELIMITER ;

DELIMITER //
drop procedure if exists get_max_id;
create procedure get_max_id()
begin
    select max(id) from Sneakers;
end //
DELIMITER ;


DELIMITER //
drop procedure if exists get_prod;
create procedure get_prod(IN p_id INT)
begin 
    select producer from Producers where id=p_id;
end //
DELIMITER ;

DELIMITER //
drop procedure if exists get_available;
create procedure get_available()
begin
    select p.producer, m.model, c.colorway, c.price 
    from
        Model m,
        Producers p,
        Colorways c
    where 
        m.producer_id=p.id AND
        c.model_id=m.id;
end //
DELIMITER ;


DELIMITER //
drop procedure if exists get_available_prod;
create procedure get_available_prod(IN prod VARCHAR(20))
begin
    declare prod_id INT;
    select id into prod_id from Producers where producer=prod;

    select p.producer, m.model, c.colorway, c.price 
    from
        Model m,
        Producers p,
        Colorways c
    where 
        m.producer_id=p.id AND
        c.model_id=m.id AND
        prod_id=p.id;
end //
DELIMITER ;

DELIMITER //
drop procedure if exists get_owned_nr;
create procedure get_owned_nr()
begin
    select count(s.id), p.producer
    from 
        Sneakers s,
        Producers p
    where
        s.producer_id=p.id
    group by s.producer_id;
end //
DELIMITER ;

DELIMITER //
drop procedure if exists get_all_nr;
create procedure get_all_nr()
begin
    select count(c.id), p.producer 
    from
        Model m,
        Producers p,
        Colorways c
    where 
        m.producer_id=p.id AND
        c.model_id=m.id
    group by p.producer;
end //
DELIMITER ;

DELIMITER //
drop procedure if exists get_spent;
create procedure get_spent()
begin
    select p.producer, s.total
    from
        Producers p,
        Spent s
    where 
        s.producer_id=p.id;
end //
DELIMITER ;

-- POPULATE

-- Producers
insert into Producers (id, producer)
values (1, "Nike");

insert into Producers (id, producer)
values (2, "Nike SB");

insert into Producers (id, producer)
values (3, "Adidas");

insert into Producers (id, producer)
values (4, "Adidas Originals");

insert into Producers (id, producer)
values (5, "Puma");

insert into Producers (id, producer)
values (6, "Jordan");

insert into Producers (id, producer)
values (7, "Asics");

-- Models
insert into Model (id, model, producer_id)
values (1, "Tekno", 1);

insert into Model (id, model, producer_id)
values (2, "Air Max 95", 1);

insert into Model (id, model, producer_id)
values (3, "Air Force 1", 1);

insert into Model (id, model, producer_id)
values (4, "Stefan Janovski", 2);

insert into Model (id, model, producer_id)
values (5, "Stefan Janovski Max", 2);

insert into Model (id, model, producer_id)
values (6, "Ultraboost", 3);

insert into Model (id, model, producer_id)
values (7, "Yung-1", 3);

insert into Model (id, model, producer_id)
values (8, "Iniki", 3);

insert into Model (id, model, producer_id)
values (9, "Superstar", 4);

insert into Model (id, model, producer_id)
values (10, "Gazelle", 4);

insert into Model (id, model, producer_id)
values (11, "Thunder Spectra", 5);

insert into Model (id, model, producer_id)
values (12, "Thunder Desert", 5);

insert into Model (id, model, producer_id)
values (13, "Retro 1", 6);

insert into Model (id, model, producer_id)
values (14, "Retro 3", 6);

insert into Model (id, model, producer_id)
values (15, "Retro 4", 6);

insert into Model (id, model, producer_id)
values (16, "Gel Lyte III", 7);

insert into Model (id, model, producer_id)
values (17, "Gel Lyte V", 7);


-- Colorways
insert into Colorways (id, model_id, colorway, price)
values (1, 1, "Blue & White", 450);

insert into Colorways (id, model_id, colorway, price)
values (2, 1, "All Black", 400);

insert into Colorways (id, model_id, colorway, price)
values (3, 1, "White & Yellow", 425);

insert into Colorways (id, model_id, colorway, price)
values (4, 2, "Silver Bullet", 850);

insert into Colorways (id, model_id, colorway, price)
values (5, 2, "Metal Gold", 900);

insert into Colorways (id, model_id, colorway, price)
values (6, 3, "Blue & White", 350);

insert into Colorways (id, model_id, colorway, price)
values (7, 3, "All White", 390);

insert into Colorways (id, model_id, colorway, price)
values (8, 4, "Black", 275);

insert into Colorways (id, model_id, colorway, price)
values (9, 4, "Black & White", 315);

insert into Colorways (id, model_id, colorway, price)
values (10, 5, "Black Janovski", 330);

insert into Colorways (id, model_id, colorway, price)
values (11, 6, "Metallic Copper", 650);

insert into Colorways (id, model_id, colorway, price)
values (12, 6, "Triple White", 700);

insert into Colorways (id, model_id, colorway, price)
values (13, 7, "Frieza", 500);

insert into Colorways (id, model_id, colorway, price)
values (14, 7, "Orange & White", 480);

insert into Colorways (id, model_id, colorway, price)
values (15, 8, "Classic Purple", 420);

insert into Colorways (id, model_id, colorway, price)
values (16, 8, "Triple Black", 420);

insert into Colorways (id, model_id, colorway, price)
values (17, 9, "Classic White", 300);

insert into Colorways (id, model_id, colorway, price)
values (18, 9, "Classic Black", 320);

insert into Colorways (id, model_id, colorway, price)
values (19, 10, "Oktoberfest", 1200);

insert into Colorways (id, model_id, colorway, price)
values (20, 10, "Blue & White", 400);

insert into Colorways (id, model_id, colorway, price)
values (21, 11, "Classic", 510);

insert into Colorways (id, model_id, colorway, price)
values (22, 11, "Cream White", 560);

insert into Colorways (id, model_id, colorway, price)
values (23, 12, "McQueen", 1300);

insert into Colorways (id, model_id, colorway, price)
values (24, 12, "Starry Blue", 870);

insert into Colorways (id, model_id, colorway, price)
values (25, 13, "Chicago", 2500);

insert into Colorways (id, model_id, colorway, price)
values (26, 13, "Lakers", 1100);

insert into Colorways (id, model_id, colorway, price)
values (27, 14, "Cement Grey", 1600);

insert into Colorways (id, model_id, colorway, price)
values (28, 14, "Cement White", 780);

insert into Colorways (id, model_id, colorway, price)
values (29, 15, "Fire Red", 750);

insert into Colorways (id, model_id, colorway, price)
values (30, 15, "Military Blue", 800);

insert into Colorways (id, model_id, colorway, price)
values (31, 16, "Japanese Garden", 600);

insert into Colorways (id, model_id, colorway, price)
values (32, 16, "Christmas Pack", 550);

insert into Colorways (id, model_id, colorway, price)
values (33, 17, "Black/Gold", 500);

insert into Colorways (id, model_id, colorway, price)
values (34, 17, "Cactus Desert", 530);

insert into Sneakers(id, producer_id, model_id, colorway_id)
values (1, 1, 2, 4);

insert into Spent(producer_id, total)
values(1, 0);

insert into Spent(producer_id, total)
values(2, 0);

insert into Spent(producer_id, total)
values(3, 0);

insert into Spent(producer_id, total)
values(4, 0);

insert into Spent(producer_id, total)
values(5, 0);

insert into Spent(producer_id, total)
values(6, 0);

insert into Spent(producer_id, total)
values(7, 0);