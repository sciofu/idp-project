import mysql.connector

producers = "select id, producer from Producers"
models = "select id, model from Model"
colorways = "select id, colorway from Colorways"

try:
    user = input("User: ")
    password = input("Password: ")

    db = mysql.connector.connect(user=user, password=password, host='mysql', port='3306', database='sneaker_collection')
    cursor = db.cursor()

    while True:
        prod = []
        mod = []
        col = []

        cursor.execute(producers)
        for x in cursor:
            prod.append(x)

        cursor.execute(models)
        for x in cursor:
            mod.append(x)

        cursor.execute(colorways)
        for x in cursor:
            col.append(x)
        

        command = input("Hello! Please state your action:\n 1. Add Sneakers\n 2. Add Producer\n 3. Add Model\n 4. Add Colorway\n Or quit :)")
        
        if command == "1":
            print("Select from the following lists a producer")
            for p in prod:
                print(p)
            prod_id = input()

            print("Select from the following lists a model")
            for m in mod:
                print(m)
            mod_id = input()

            print("Select from the following lists a colorway")
            for c in col:
                print(c)
            col_id = input()

            new_id = input("Type the id of the new sneakers:\n")

            cursor.execute("insert into Sneakers(id, producer_id, model_id, colorway_id) values(%s, %s, %s, %s)", (new_id, prod_id, mod_id, col_id))

            cursor.execute("select * from Sneakers")
            for x in cursor:
                print(x)

        elif command == "2":
            new_prod = input("Type the name of the new producer:\n")
            new_id = input("Type an id for the new producer:\n")

            cursor.execute("insert into Producers (id, producer) values (%s, %s)", (new_id, new_prod))

            cursor.execute("select * from Producers")
            for x in cursor:
                print(x)

        elif command == "3":
            for p in prod:
                print(p)
            prod_id = input("Tpye the id of the producer:\n")
            
            new_model = input("Type the name of the new model:\n")
            new_id = input("Type the id for the new model:\n")

            cursor.execute("insert into Model (id, model, producer_id) values (%s, %s, %s)", (new_id, new_model, prod_id))
            
            cursor.execute("select * from Model")
            for x in cursor:
                print(x)

        elif command == "4":
            for m in mod:
                print(m)
            model_id = input("Type the id of the model:\n")

            new_col = input("Type the name of the new colorway:\n")
            price = input("Type the price of the colorway:\n")
            new_id = input("Type the id of the new colorway:\n")

            cursor.execute("insert into Colorways (id, colorway, model_id, price) values (%s, %s, %s, %s)", (new_id, new_col, model_id, price))
            
            cursor.execute("select * from Colorways")
            for x in cursor:
                print(x)

        elif command == "quit":
            break
        else:
            print('Invalid command')

except mysql.connector.Error as e:
    print(e)
    db.rollback()
finally:
    cursor.close()
    db.close()
