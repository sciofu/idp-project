from flask import Flask, request, jsonify, Response
import mysql.connector
from flask_cors import CORS, cross_origin

app = Flask(__name__)
cors = CORS(app, resources={r"*": {"origins": "*"}})

@app.route('/')
def hello():
    return "Hello World!??"


@app.route('/collection', methods=["GET"])
@cross_origin()
def getAll():
    try:
        # return "fmm"
        db = mysql.connector.connect(user='root', password='root', host='mysql', port='3306', database='sneaker_collection')
        cursor = db.cursor()
        
        cursor.callproc('get_collection', [])
        for r in cursor.stored_results():
            res = r.fetchall()

        return jsonify(res), 200
    except mysql.connector.Error as e:
        print(e)
        db.rollback()
        return e
    finally:
        cursor.close()
        db.close()


@app.route('/collection', methods=["POST"])
def addSneakers():
    payload = request.get_json(silent=True)
    payload = payload["body"]
    print(payload);
    producer = payload["producer"]
    model = payload["model"]
    colorway = payload["color"]

    try:
        db = mysql.connector.connect(user='root', password='root', host='mysql', port='3306', database='sneaker_collection')
        cursor = db.cursor()

        new_id = 0
        cursor.callproc('get_max_id', [])
        for r in cursor.stored_results():
            new_id = r.fetchall()[0][0]
            if (new_id != None):
                new_id = new_id + 1
            else:
                new_id = 0

        cursor.callproc('add_sneakers', [new_id, producer, model, colorway])
        db.commit()

        return "Added", 200
    except mysql.connector.Error as e:
        print(e)
        db.rollback()
        return "Error accessing database"
    finally:
        cursor.close()
        db.close()

@app.route('/collection/prod/<name>', methods=["GET"])
def getByProd(name):
    payload = request.get_json(silent=True)
    
    try:
        db = mysql.connector.connect(user='root', password='root', host='mysql', port='3306', database='sneaker_collection')
        cursor = db.cursor()

        
        cursor.callproc('get_by_prod', [name])
        for r in cursor.stored_results():
            res = r.fetchall()

        return jsonify(res), 200
    except mysql.connector.Error as e:
        print(e)
        db.rollback()
        return "Error accessing database"
    finally:
        cursor.close()
        db.close()

@app.route('/available/prod/<name>', methods=["GET"])
def getAvailableByProd(name):
    payload = request.get_json(silent=True)
    
    try:
        db = mysql.connector.connect(user='root', password='root', host='mysql', port='3306', database='sneaker_collection')
        cursor = db.cursor()

        
        cursor.callproc('get_available_prod', [name])
        for r in cursor.stored_results():
            res = r.fetchall()

        return jsonify(res), 200
    except mysql.connector.Error as e:
        print(e)
        db.rollback()
        return "Error accessing database"
    finally:
        cursor.close()
        db.close()


@app.route('/available', methods=["GET"])
def getAvailable():
    payload = request.get_json(silent=True)
    
    try:
        db = mysql.connector.connect(user='root', password='root', host='mysql', port='3306', database='sneaker_collection')
        cursor = db.cursor()

        
        cursor.callproc('get_available', [])
        for r in cursor.stored_results():
            res = r.fetchall()

        return jsonify(res), 200
    except mysql.connector.Error as e:
        print(e)
        db.rollback()
        return "Error accessing database"
    finally:
        cursor.close()
        db.close()


@app.route('/available/nr', methods=["GET"])
def getAvailableNr():
    payload = request.get_json(silent=True)
    
    try:
        db = mysql.connector.connect(user='root', password='root', host='mysql', port='3306', database='sneaker_collection')
        cursor = db.cursor()

        
        cursor.callproc('get_all_nr', [])
        for r in cursor.stored_results():
            res = r.fetchall()

        return jsonify(res), 200
    except mysql.connector.Error as e:
        print(e)
        db.rollback()
        return "Error accessing database"
    finally:
        cursor.close()
        db.close()

@app.route('/collection/nr', methods=["GET"])
def getCollectionNr():
    payload = request.get_json(silent=True)
    
    try:
        db = mysql.connector.connect(user='root', password='root', host='mysql', port='3306', database='sneaker_collection')
        cursor = db.cursor()

        cursor.callproc('get_owned_nr', [])
        for r in cursor.stored_results():
            res = r.fetchall()

        return jsonify(res), 200
    except mysql.connector.Error as e:
        print(e)
        db.rollback()
        return "Error accessing database"
    finally:
        cursor.close()
        db.close()

@app.route('/collection/spent', methods=["GET"])
def getSpent():
    payload = request.get_json(silent=True)
    
    try:
        db = mysql.connector.connect(user='root', password='root', host='mysql', port='3306', database='sneaker_collection')
        cursor = db.cursor()

        cursor.callproc('get_spent', [])
        for r in cursor.stored_results():
            res = r.fetchall()

        return jsonify(res), 200
    except mysql.connector.Error as e:
        print(e)
        db.rollback()
        return "Error accessing database"
    finally:
        cursor.close()
        db.close()
